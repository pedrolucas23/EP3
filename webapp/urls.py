from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^quarto_1/$', views.quarto_1, name='quarto_1'),
    url(r'^quarto_2/$', views.quarto_2, name='quarto_2'),
]
