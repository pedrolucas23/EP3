from django.shortcuts import render
from django.http import HttpResponse
import sys
import RPi.GPIO as GPIO
import time

# Create your views here.

def index(request):
    return render(request, 'webapp/home.html')

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
StepPins = [21, 20, 16, 12]
LedPins = [17, 18, 2, 3]
#A segunda janela nao foi implementada por nao haver outro motor de passo

for pin in StepPins:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, False)

for pin in LedPins:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, False)

Seq = [[1,0,0,0],
       [1,1,0,0],
       [0,1,0,0],
       [0,1,1,0],
       [0,0,1,0],
       [0,0,1,1],
       [0,0,0,1],
       [1,0,0,1]]

StepCount = len(Seq) - 1

WaitTime = 1/float(400)

def quarto_1(request):
    if 'on' in request.POST:
        GPIO.output(18, GPIO.HIGH)
    elif 'off' in request.POST:
        GPIO.output(18, GPIO.LOW)
    elif "on2" in request.POST:
        GPIO.output(17, GPIO.HIGH)
    elif "off2" in request.POST:
        GPIO.output(17, GPIO.LOW)
    elif "open" in request.POST:
        StepDir = 2
        StepCounter = 0
        i = 2500

        while i>0:
            for pin in range(0, 4):
                xpin = StepPins[pin]
                if Seq[StepCounter][pin]!=0:
                    GPIO.output(xpin, True)
                else:
                    GPIO.output(xpin, False)

            StepCounter += StepDir

            if (StepCounter >= StepCount):
                StepCounter = 0
            if (StepCounter<0):
                StepCounter = StepCount

            time.sleep(WaitTime)
            i -= 1

    elif "close" in request.POST:
        StepDir = -2
        StepCounter = 0
        i = 2500

        while i>0:
            for pin in range(0, 4):
                xpin = StepPins[pin]
                if Seq[StepCounter][pin]!=0:
                    GPIO.output(xpin, True)
                else:
                    GPIO.output(xpin, False)

            StepCounter += StepDir

            if (StepCounter >= StepCount):
                StepCounter = 0
            if (StepCounter<0):
                StepCounter = StepCount

            time.sleep(WaitTime)
            i -= 1
        
    return render(request, 'webapp/quarto1.html')

def quarto_2(request):
    if 'on3' in request.POST:
        GPIO.output(3, GPIO.HIGH)
    elif 'off3' in request.POST:
        GPIO.output(3, GPIO.LOW)
    elif "on4" in request.POST:
        GPIO.output(2, GPIO.HIGH)
    elif "off4" in request.POST:
        GPIO.output(2, GPIO.LOW)
            
    return render(request, 'webapp/quarto2.html')
