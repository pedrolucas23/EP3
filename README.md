#    Orientação a Objetos 2/2016 (UnB - Gama)

##   EP3

###  Alunos: Pedro Lucas Pinheiro de Souza (13/0035581)
###          Kaio Diego de Araujo Coelho   (12/0123673)

Este projeto consiste na programação de um website em django python em conunto com funcionalidades de uma raspberry, simulando uma automação residencial

#### Como Compilar e Executar

Para compilar e executar o programa em uma raspberry pi com o sistema operacional RASPBIAN, siga as seguintes instruções:


* Baixe o arquivo em sua raspberry, ou em seu computador e transfira para a raspberry, então acesse-a;
* Caso não seja possível acessar a raspberry por um teclado, um mouse e um monitor, é possível o acesso via comando SSH com o terminal do linux (onde os valores x correspondem a endereço ip local da raspberry):
	**ssh pi@xxx.xxx.xx.xx**
* Ao acessar a raspberry, é necessário a preparação do ambiente instalando alguns programas necessários para o correto funcionamento do site. Execute os seguintes comandos:
	**sudo apt-get install python-mysqldb -y**
	**sudo apt-get install apache2 -y**
	**sudo apt-get install mysql-server mysql-client -y**
	**sudo apt-get install php5 libapache2-mod-php5 php5-mysql -y**
	**sudo service apache2 restart**
	**sudo apt-get install phpmyadmin -y**
	**sudo nano /etc/apache2/apache2.conf**
ao abrir o editor de terminal, insira a seguinte linha na primeira linha do editor:
	**Include /etc/phpmyadmin/apache.conf**
então continue executando os comandos de configuração:
	**sudo service apache2 restart**
	**sudo apt-get install python-setuptools -y**
	**wget  https://bootstrap.pypa.io/get-pip.py**
	**sudo python get-pip.py**
	**sudo rm -rf get-pip.py**
	**sudo pip install Django**
* Após configurado o ambiente para a raspberry, encontre o diretório do projetoe acesse-o;
* No diretório do projeto, digite no terminal:
	**python manage.py runserver**
* Note que o site apenas funcionará na própria raspberry por ser apenas local;
* Para que esse site funcione dentro da rede ao qual a raspberry esteja ligada, identifique o hostname utilizado pela raspberry com o comando:
	**hostname -I**
* Acesse a pasta dentro do projeto que possui o mesmo nome da pasta principal do projeto e edite o arquivo settings.py;
* Na linha onde encontra-se: ALLOWED_HOSTS = [], acrescente a string:
	**u'xxx.xxx.xx.xx'**
ondes os valores de x correspondem ao hostname encontrado;
* Após executado todos os comandos acima, volte a pasta principal e digite novamente no console o seguinte comando:
	**python manage.py runserver xxx.xxx.xx.xx:8000**
onde os valores de x correspondem ao hostname atribuido pela raspberry;
* Para observar o site, acesse através de qualquer browser:
	**http://xxx.xxx.xx.xx:8000/**
onde os valores de x correspondem ao hostname atribuido pela raspberry.

